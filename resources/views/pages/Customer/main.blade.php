<x-app-layout>
	<ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="/dashboard">Home</a>
      </li>
      <li class="breadcrumb-item active">Customer</li>
    </ol>

    <div class="card">
    	<div class="card-header">
    		<h4 class="float-left">Customer List</h4>
    		<button class="btn btn-success add-customer float-right">Add Customer</button>
    	</div>
    	<div class="card-body" id="content">
    		
    	</div>
    	<div class="card-footer">
    		
    	</div>
    </div>

    <!-- Modal -->
	<div class="modal fade" id="addCustomer" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="staticBackdropLabel">Add Customer</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			  <div id="addCustomerMessage"></div>
			  @csrf
			  <input type="hidden" name="customer_id" id="customer-id">
			  <div class="customer-image-thumbnail">

				  <img id="show-customer_profile" class="customer-image" src="{{asset('images/user-icon.png')}}">
			  </div>
			  <div class="text-center">
				  <label>Customer Profile:</label>
				  <input type="file" name="customer_profile" id="customer_profile" class="form-control-file">
				  <br>
				  <small>Customer Profile is optional.</small>
			  </div>

			  <div class="form-group">
				  <label for="">Customer Name</label>
				  <input type="text" class="form-control" name="name" value="" id="customer-name" placeholder="Enter Customer name here..."
						 required>
			  </div>
			  <div class="form-group">
				  <label for="">Customer Phone</label>
				  <input type="text" class="form-control" name="customer-phone" value="" id="customer-phone" placeholder="Enter Customer Phone Number here..."
						 required>
			  </div>
			  <div class="form-group">
				  <label for="">Customer Email</label>
				  <input type="text" class="form-control" name="email" value="" id="customer-email" placeholder="Enter Customer Email Address here...">
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Understood</button>
	      </div>
	    </div>
	  </div>
	</div>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$(".add-customer").on('click', function(){
                $("#addCustomer").modal('show');
    		});
    	});

		$("#customer_profile").on('change',function(e){
			$("#show-customer_profile").attr('src', URL.createObjectURL(e.target.files[0]));
		});
    </script>
</x-app-layout>