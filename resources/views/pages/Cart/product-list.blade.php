<div class="card">
	<div class="card-header">
		<h2></h2>
		
	</div>
	<div class="card-body">
		<table class="table table-sm">
			<thead>
				<tr>
					<th>Id</th>
					<th>Product Name & items</th>
					<th>cost/item</th>
					<th>Total Price</th>
				</tr>
			</thead>
			<tbody>
				<?php $grand_price = 0  ?>
				@foreach($products as $key => $product)
				<tr>
					<td>{{ $key + 1 }}</td>
					<td>{{ $product->products->name }} x {{ $product->no_of_items }}</td>
					<td>{{ $product->products->sale_price }} </td>
					<td>Rs.{{ $product->price }}</td>
				</tr>
				<?php  $grand_price = $grand_price +  $product->price ?>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="card-footer">
		<div class="row">
			<div class="col-12 mb-4">
				<label>Discount:</label>
				<input type="number" name="discount" id="discount" value="0" class="form-control">
			</div>
			<div class="col-8">
				<h4 class="">Grand Price: </h4>
			</div>
			<div class="col-4">
				<span class="d-none actual-price">{{ number_format($grand_price, 2) }}</span>
				<h4>Rs. <span id="total-price">{{ number_format($grand_price, 2) }}</span></h4>
			</div>
			<hr>
			@if (count($products) > 0)
				<button target="_blank" class="btn btn-success btn-cash-recived">Received Cash</button>
			@endif
		</div> 
	</div>
</div>