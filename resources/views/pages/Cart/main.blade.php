<x-app-layout>
	<div class="card mb-4">
		<div class="card-header">
			<h2 class="font-weight-bold">Add To Cart</h2>
			<button class="btn btn-danger btn-clear-cart">Clear Cart</button>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-6">
					<div id="message">
					</div>
					{{--<form id="add-to-cart">--}}
						{{--@csrf--}}
						{{--<label>Select Product:</label>--}}
						{{--<div class="form-group row">--}}
							{{--<div class="col-8">	--}}
								{{--<select name="product" class="form-control select2">--}}
									{{--<option selected disabled>Choose Product...</option>--}}
									{{--@foreach($products as $product)--}}
										{{--<option value="{{ $product->id }}">{{ $product->name }}</option>--}}
									{{--@endforeach--}}
								{{--</select>--}}
							{{--</div>--}}
							{{--<div class="col-4">--}}
								{{--<input type="number" name="items" placeholder="No of items" class="form-control">--}}
							{{--</div>--}}
							{{--<div class="col-12 mt-2">--}}
								{{--<button class="btn text-center btn-success btn-block">Add to cart</button>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</form>--}}
					{{--<h2>Product</h2>--}}
					<div class="sale-search-main">
						<div class="row">
							<div class="col-3">
								<div class="sale-search-logo">
									<img src="{{asset('images/resource-01.png')}}"/>
								</div>
							</div>
							<div class="col-9">
								<form class="form-inline sale-search">
									<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
								</form>
							</div>
						</div>
					</div>
					
					<div class="product-list-main product-type-list">
						<div class="row">
								@foreach($products as $product)
								<div class="col-4 product-type" data-id="{{ $product->id }}">
									<div class="product-widget">
										<a href="#">
											<img src="{{asset('images/resource-01.png')}}"/>
											<h2>{{ $product->name }}</h2>
										</a>
									</div>
									{{--<option value="{{ $product->id }}">{{ $product->name }}</option>--}}
								</div>
								@endforeach
						</div>
					</div>
					<div class="product-list-main product-list d-none">
						<div class="row products">
						</div>
					</div>
				</div>
				<div class="col-6" id="product-list">
					<div class="text-center">
						<div class="spinner-grow" style="width: 8rem; height: 8rem;" role="status">
					  		<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			$(".select2").select2();
			function loadProductList()
			{
				$.ajax({
					url: '/show-cart-product-list',
					method: 'get',
					beforeSend:function()
					{
					
					},
					success:function(data)
					{
						$("#product-list").html('');
						$("#product-list").append(data);	
					}
				});
			}
			$(".btn-clear-cart").on('click', function(){
				$.ajax({
					url: '/clear-cart',
					method: 'get',
					beforeSend:function()
					{},
					success:function(data)
					{
						if(data.response == 1)
						{
							loadProductList();
						}
					}
				});
			});
			loadProductList();
			$(".product-type").on('click',function(){
				$(".product-type-list").addClass('d-none');
				$.ajax({
					url: '/show-products',
					method: 'get',
					data: {
						'product_type':$(this).attr('data-id'),
					},
					beforeSend:function(){
						$(".product-list").removeClass('d-none');
						var html = '<div class="d-flex m-5 align-items-center">';
					    html += '<strong>Loading...</strong>';
						html += '<div class="spinner-border ml-auto" role="status" aria-hidden="true">';
						html += '</div></div>';
						// html += '</div>';
						// $(".products").html(html);
					},
					success:function(data)
					{
						if(data.response == 1)
						{
							// $(".products").html('');
							$.each(data.products, function(i,v){
								var product = '<div class="col-4 product-item" data-id="'+ v.id +'">';
								product += '<div class="product-widget">';
								product += '<a href="#">';
								product += '<img src="'+v.product_pic+'"/>';
								product += '<h2>'+ v.name +'</h2>';
								product += '</a>';
								product += '</div>';
								product += '</div>';
								$(".products").append(product);
							});
						}
						else{
							$(".products").html(data.message);
							$(".products").addClass(data.class);
						}
					}
				});
			});
			$(".product-item").on('click', function(){
				alert("dajlkdaslkda");
			});
			$(".product-item").on('click', function(){
				// e.preventDefault();
				alert($(this).attr('data-id'));
				$.ajax({
					url : '/add-to-cart',
					method: 'POST',
					data: {
						'product': $(this).attr('data-id'),
						'items': 1,
					},
					processData:false,
					dataType:'JSON',
					contentType:false,
					cache:false,
					beforeSend:function()
					{
						$("#message").html('');
						$("#message").removeClass();
					},
					success:function(data)
					{
						if(data.response == 1)
						{
							$("#message").append(data.message);
							$("#message").addClass(data.class);
							$("#add-to-cart")[0].reset();
							loadProductList();
							setTimeout(function(){
								$("#message").html('');
						        $("#message").removeClass();
							}, 5000);	
						}
						else{
							$("#message").append(data.error);
							$("#message").addClass(data.class);
							setTimeout(function(){
								$("#message").html('');
						        $("#message").removeClass();
							}, 5000);	
						}
						
					}
				});
			});
			$("#add-to-cart").on('submit', function(e){
				e.preventDefault();
				$.ajax({
					url : '/add-to-cart',
					method: 'POST',
					data: new FormData(this),
					processData:false,
					dataType:'JSON',
					contentType:false,
					cache:false,
					beforeSend:function()
					{
						$("#message").html('');
						$("#message").removeClass();
					},
					success:function(data)
					{
						if(data.response == 1)
						{
							$("#message").append(data.message);
							$("#message").addClass(data.class);
							$("#add-to-cart")[0].reset();
							loadProductList();
							setTimeout(function(){
								$("#message").html('');
						        $("#message").removeClass();
							}, 5000);	
						}
						else{
							$("#message").append(data.error);
							$("#message").addClass(data.class);
							setTimeout(function(){
								$("#message").html('');
						        $("#message").removeClass();
							}, 5000);	
						}
						
					}
				});
			});
			$(document).on('keyup', '#discount', function(){
				if($(this).val() > 0)
				{
					$("#total-price").html($(".actual-price").html() - $(this).val());
					$("#total-price").number( true, 2 );
				}
				else{
					$("#total-price").html($(".actual-price").html());
				}
				
			});
			$(document).on('click', '.btn-cash-recived', function(){
				$.ajax({
					url : '/cash-recived',
					method: 'get',
					data: {
						'discount': $("#discount").val(),
					},
					beforeSend:function()
					{
						$("#message").html('');
						$("#message").removeClass();
					},
					success:function(data)
					{
						if(data.response == 1)
						{
							window.open("/print-invoice", "_blank");
							loadProductList();
						}
						else
						{
							
							$("#message").append(data.error);
							$("#message").addClass(data.class);
						}
								
					}
				});
			});
		});
	</script>
</x-app-layout>