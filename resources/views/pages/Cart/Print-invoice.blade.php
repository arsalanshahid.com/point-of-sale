<!DOCTYPE html>
<html>
<head>
	<title>Print Invoice</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
<body>
	<div class="container">
		<div class="row m-2">
			<div class="invoice-header">
				<div class="invoice-logo">
					<img src="{{asset('images/point-of-sales-logo.png')}}"/>
				</div>
				<div class="invoice-header-info">
					<h2>NSS Point of Sale</h2>
					<p><strong>Address:</strong>New Shalimar Colony Street no 3 Jatoi House</p>
					<p><strong>Phone:</strong>+92 300 1234567</p>
				</div>
			</div>
			<div class="invoice-item-list">
				<ul>
					@foreach($data->invoice_details as $key => $product)
					<li>
						<div class="product-item">
							<p>{{ $product->no_of_items }} x {{ $product->products->name }} </p>
						</div>
						<div class="product-price">

							<p>Rs. {{number_format("$product->total_cost",2)}} </p>
						</div>
					</li>
					@endforeach
				</ul>
				<div class="total-price-main">
					<div class="total-heading">
						<p>Total</p>
					</div>
					<div class="total-price">
						<p>Rs. {{number_format("$data->final_amount",2)}}</p>
					</div>
				</div>
			</div>
			<div class="invoice-footer">
				<ul>
					<li>

						<p><strong>Date:</strong> {{Carbon\Carbon::now() }}</p>
					</li>
					<li>
						<p><strong>Order:</strong> {{ $data->Invoice_no }}</p>
					</li>
					<li>
						<p><strong>Customer:</strong> Customer Name</p>
					</li>
					<li>
						<p><strong>Cashier:</strong> Cashier Name</p>
					</li>
				</ul>
				<span>Thanks for your purchase</span>
			</div>
			{{--<div class="col-6 mt-4 h4">--}}
				{{--<span class="font-weight-bold ">Invoice #: </span>{{ $data->Invoice_no }}--}}
			{{--</div>--}}
			{{--<div class="col-12 mt-1">--}}
				{{--<div class="row">--}}
					{{--<div class="col-4">--}}
						{{--<span class="font-weight-bold">Total Price: </span>--}}
					{{--</div>--}}
					{{--<div class="col-4">--}}
						{{--Rs.{{ $data->sales_amount }}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="row">--}}
					{{--<div class="col-4">--}}
						{{--<span class="font-weight-bold ">Discount Amount: </span>--}}
					{{--</div>--}}
					{{--<div class="col-4">--}}
						{{--Rs.{{ $data->discount }}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="row">--}}
					{{--<div class="col-4">--}}
						{{--<span class="font-weight-bold ">Payable Amount: </span>--}}
					{{--</div>--}}
					{{--<div class="col-4">--}}
						{{--Rs.{{ $data->final_amount }}--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		</div>
		{{--<hr>--}}
		{{--<h4 class="text-center">Product Detail</h4>--}}
		{{--<div class="table-responsive">--}}
			{{--<table class="table table-striped">--}}
				{{--<thead>--}}
					{{--<tr>--}}
						{{--<th>#</th>--}}
						{{--<th>Product & items</th>--}}
						{{--<th>Cost/Item</th>--}}
						{{--<th>Total Price</th>--}}
					{{--</tr>--}}
				{{--</thead>--}}
				{{--<tbody>--}}
					{{--@foreach($data->invoice_details as $key => $product)--}}
					{{--<tr>--}}
						{{--<td>{{ $key+1 }}</td>--}}
						{{--<td>{{ $product->products->name }} x {{ $product->no_of_items }}</td>--}}
						{{--<td>Rs.{{ $product->products->sale_price }}</td>--}}
						{{--<td>Rs.{{ $product->total_cost }}</td>--}}
					{{--</tr>--}}
					{{--@endforeach--}}
				{{--</tbody>--}}
			{{--</table>--}}
		{{--</div>--}}
		{{--<hr>--}}
	</div>

	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>