<x-admin-layout>
	<!-- Breadcrumbs-->
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="/home">Home</a>
	    </li>
	    <li class="breadcrumb-item active">Branches</li>
	  </ol>

	  <div class="card">
	  	<div class="card-header">
	  		<h2 class="text-primary font-weight-bold float-left">Branch List</h2>
	  		<a class="btn btn-success float-right" href="/add-branch">Add Branch</a>
	  	</div>
	  	<div class="card-body">
	  		<div class="table-responsive" id="table2">			
	  		</div>
	  	</div>
	  	<div class="card-footer"></div>
	  </div>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		var spinner = '<div class="d-flex m-5 align-items-center">';
		    spinner += '<strong>Loading...</strong>';
			spinner += '<div class="spinner-border ml-auto" role="status" aria-hidden="true">';
			spinner += '</div></div>';
	  		$.ajax({
	  			url: 'show-branches',
	  			method: 'get',
	  			beforeSend:function(){
	  				$("#table2").html(spinner);
	  			},
	  			success:function(data){
	  				// $.each(data,function(i,v){
	  				// 	var content = '<tr>';
	  				// 	content +=  '<td>'+ i +'</td>';
	  				// 	content +=  '<td>'+ v.branch_name +'</td>';
	  				// 	content +=  '<td>' + v.city + '</td>';
	  				// 	content += '<td>';
	  				// 	content += '<button class="btn btn-success">Edit</button>';
	  				// 	content += '<button class="btn btn-danger">Remove</button>';
	  				// 	content += '</td>';
	  				// 	content += '</tr>';
	  				// 	$("#content").append(content);
	  				// });
	  				$("#table2").html(data);
	  			}
	  		});
	  	});
	  </script>
</x-admin-layout>