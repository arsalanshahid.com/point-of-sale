<x-admin-layout>
	<div class="card">
		<div class="card-header">
			<h2 class="text-primary font-weight-bold">Add Branches</h2>
		</div>
		<div class="card-body">
			<form method="POST" id="add-branch"> 
				<div id="addMessage"></div>
				@csrf
				<label>Branch Name:</label>
				<input type="text" name="branch_name" class="form-control form-control-lg">
				<label>Branch City:</label>
				<input type="text" name="branch_city" class="form-control form-control-lg">
				<br>
				<br>
				<button class="btn btn-success btn-block">Save</button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#add-branch").on('submit',function(e){
				e.preventDefault();
				$.ajax({
					url: 'add-branch',
					method: 'POST',
					data: new FormData(this),
					processData:false,
					dataType: 'JSON',
					contentType:false,
					cache:false,
					beforeSend:function(){
						$("#addMessage").html('');
						$("#addMessage").removeClass();
					},
					success:function(data){
						if(data.response == 0){
							$.each(data.errors,function(i,v){
								$("#addMessage").append(v+ '<br>');
							})
							$("#addMessage").addClass(data.class);
						}else{
							$("#addMessage").append(data.message);
							$("#addMessage").addClass(data.class);
						}
					}
				});
			});
		});
	</script>
</x-admin-layout>