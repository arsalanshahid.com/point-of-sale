<table class="table table-striped table-bordered" id="dataTable">
	<thead>
		<tr>
			<th>#</th>
			<th>Branch</th>
			<th>City</th>
			<th>Name</th>
			<th>Email</th>
			<th>Password</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody id="content">
		@foreach($accounts as $key => $account )
		<tr>
			<td>{{ $key + 1 }}</td>
			<td>{{ $account->branches->branch_name }}</td>
			<td>{{ $account->branches->city }}</td>
			<td>{{ $account->name }}</td>
			<td>{{ $account->email }}</td>
			<td>{{ $account->password }}</td>
			<td>
			<button class="btn btn-success">Edit</button>
			<button class="btn btn-danger">Remove</button>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/datatables-demo.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>