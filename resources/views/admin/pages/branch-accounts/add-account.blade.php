<x-admin-layout>
	<div class="card">
		<div class="card-header">
			<h2 class="text-primary font-weight-bold">Add Branches</h2>
		</div>
		<div class="card-body">
			<form method="POST" id="add-branch-account"> 
				<div id="addMessage"></div>
				@csrf
				<label>Branch:</label>
				<select class="form-control custom-select" name="branch">
					@foreach($branches as $branch)
						@if($branch->id == 1)
						@else
							<option value="{{ $branch->id }}">{{ $branch->branch_name }} -- {{ $branch->city }}</option>
						@endif
					@endforeach					
				</select>
				<label>Name:</label>
				<input type="text" name="name" class="form-control form-control-lg">
				<label>Email:</label>
				<input type="email" name="email" class="form-control form-control-lg">
				<label>Password:</label>
				<input type="text" name="password" class="form-control form-control-lg">
				<br>
				<br>
				<button class="btn btn-success btn-block">Save</button>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#add-branch-account").on('submit',function(e){
				e.preventDefault();
				$.ajax({
					url: 'add-branch-account',
					method: 'POST',
					data: new FormData(this),
					processData:false,
					dataType: 'JSON',
					contentType:false,
					cache:false,
					beforeSend:function(){
						$("#addMessage").html('');
						$("#addMessage").removeClass();
					},
					success:function(data){
						if(data.response == 0){
							$.each(data.errors,function(i,v){
								$("#addMessage").append(v+ '<br>');
							})
							$("#addMessage").addClass(data.class);
						}else{
							$("#addMessage").append(data.message);
							$("#addMessage").addClass(data.class);
							$("#add-branch-account")[0].reset();
						}
					}
				});
			});
		});
	</script>
</x-admin-layout>