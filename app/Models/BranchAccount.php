<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchAccount extends Model
{
    use HasFactory;

    protected $fillable = [
    	'brnach_id',
    	'name',
    	'email',
    	'password'
    ];

    public function branches(){
    	return $this->belongsTo('App\Models\Branch', 'branch_id');
    }
}
