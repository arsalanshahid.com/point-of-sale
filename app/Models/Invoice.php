<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use App\Models\InvoiceDetail;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable =[
    	'invoice_no',
    	'sales_amount',
    	'discount',
    	'final_amount',
    ];


    public function invoice_details()
    {
    	return $this->hasMany('App\Models\InvoiceDetail', 'invoice_id');
    }
    
    public function products()
    {
    	$this->belongs('App\Models\Products', 'product_id');
    }
}

