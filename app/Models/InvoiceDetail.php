<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    use HasFactory;
    protected $fillable =[
    	'invoice_id',
    	'product_id',
    	'no_of_items',
    	'cost_per_item',
    	'total_cost',
    ];

    public function products()
    {
    	return $this->belongsTo('App\Models\Product', 'product_id');
    }
}

