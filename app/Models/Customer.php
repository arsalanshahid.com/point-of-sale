<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
    	'customer_profile',
    	'name',
    	'phone_number',
    	'email',
    	'total_purchase'
    ];
}
