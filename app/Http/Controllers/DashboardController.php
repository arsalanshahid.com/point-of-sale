<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Branch;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
	public function index()
	{
		if(Auth::user()->roles->roles == 'admin')
		{
			$data = [
				'branches' => Branch::count(),
			];
			return view('admin.dashboard', compact('data'));

		}
		else
		{
			$data = [
			'short-items' => Product::with('brands', 'vendors')
								->where('stock', '<', 0)->count(),
			];
			return view('dashboard',compact('data'));
			// return redi('dashboard');
		}

	}
    public function show_items()
    {
    	$data = Product::orderBy('id', 'DESC')->where('stock', '<', 0)->get();
    	return view('pages.Dashboard-pages.short-items',compact('data'));
    }
}
