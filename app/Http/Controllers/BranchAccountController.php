<?php

namespace App\Http\Controllers;

use App\Models\BranchAccount;
use App\Models\User;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class BranchAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.pages.branch-accounts.main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $branches = Branch::orderBy('id', 'DESc')->get();
        return view('admin.pages.branch-accounts.add-account',compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'branch'        => 'required|unique:branch_accounts,branch_id',
            'name'        => 'required',
            'email'       => 'required|email|unique:branch_accounts|unique:users',
            'password'    => 'required|min:8',
        ]);
        if($validator->fails()){
            $data = [
                'response' => 0,
                'errors'   => $validator->errors()->all(),
                'class'    => 'alert alert-danger',
            ];
            return response()->json($data);
        }
        else{
            $data = new BranchAccount;
            $data->branch_id = $request->branch;
            $data->name   =  $request->name;
            $data->email  = $request->email;
            $data->password = $request->password;
            $data->save();
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role_id'  => 2,
                'branch_id'=> $request->branch,
            ]);
            $data = [
                'response' => 1,
                'message'   => 'Branch data saved successsfully',
                'class'    => 'alert alert-success',
            ];
            return response()->json($data);   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BranchAccount  $branchAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BranchAccount $branchAccount)
    {
        //
        $accounts = BranchAccount::with('branches')->orderBy('id', 'DESC')->get();
        return view('admin.pages.branch-accounts.show',compact('accounts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BranchAccount  $branchAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(BranchAccount $branchAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BranchAccount  $branchAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BranchAccount $branchAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BranchAccount  $branchAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(BranchAccount $branchAccount)
    {
        //
    }
}
