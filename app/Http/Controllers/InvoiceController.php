<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Cart;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return response()->json();
        if(Cart::count()>0)
        {
            $last_invoice_id = Invoice::select('id')->latest()->first();
            if($last_invoice_id)
            {
                $invoice_id = $last_invoice_id->id + 1;
            }
            else{
                $invoice_id =  1;
            }
            $sales_price = Cart::sum('price');
            $data = new Invoice;
            $data->invoice_no   = $invoice_id;
            $data->sales_amount = $sales_price;
            $data->discount  = $request->discount;
            $data->final_amount = $sales_price - $request->discount;
            if($data->save())
            {
                $products = Cart::with('products')->get();
                foreach($products as $product)
                {
                    $data = new InvoiceDetail;
                    $data->invoice_id = $invoice_id;
                    $data->product_id = $product->product_id;
                    $data->no_of_items = $product->no_of_items;
                    $data->cost_per_item = $product->products->sale_price;
                    $data->total_cost = $product->no_of_items * $product->products->sale_price;
                    $check = $data->save();
                }
                if($check)
                {
                    $check = Cart::truncate();
                    if($check)
                    {
                        $data = [
                        'response' => 1,
                       ];
                        return response()->json($data);
                    }
                }
            }
            
            
        }
        else{
            $data = [
                'response' => 0,
            ];
            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function print_invoice()
    {
        $data = Invoice::with('invoice_details')->latest()->first();
        return view('pages.Cart.Print-invoice', compact('data'));
    }
}
