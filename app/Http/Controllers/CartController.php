<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = ProductType::orderBY('id', 'DESC')->get();
        return view('pages.Cart.main',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'items'    => 'required|gte:1',
        ]);
        if($validator->fails())
        {
            $data = [
                'response' => 0,
                'error'    => $validator->errors()->all(),
                'class'    => 'alert alert-danger',
            ];
            return response()->json($data);

        }
        else{
           $product = Product::where('id', $request->product)->first();
           $check = Cart::where('product_id', $request->product)->first();
           if($check)
           {
                if($request->items > $product->stock)
                {
                    $data = [
                        'response' => 0,
                        'error'    => 'There is no avaible stock more.',
                        'class'    => 'alert alert-danger' 
                    ];
                    return response()->json($data);
                }
                elseif ($check->no_of_items >= $product->stock) {
                    
                    $data = [
                        'response' => 0,
                        'error'    => 'There is no avaible stock more.',
                        'class'    => 'alert alert-danger' 
                    ];
                    return response()->json($data);
                 } 
                else
                { 
                    $update = Cart::where('product_id', $request->product)
                                ->update([
                                    'no_of_items' => $check->no_of_items + $request->items,
                                    'price'       => $check->price + ( $request->items * $product->sale_price)
                                ]);
                    if($update)
                    {
                    $data = [
                        'response' => 1,
                        'message'  => 'Product Added to cart',
                        'class'    => 'alert alert-success',
                    ];
                    return response()->json($data);
                    }
                }
           }
           else
           {
                if($request->items > $product->stock)
                {
                    $data = [
                        'response' => 0,
                        'error'    => 'The total available stock is '. $product->stock .'.',
                        'class'    => 'alert alert-danger' 
                    ];
                    return response()->json($data);
                }
                else
                {
                   $data = new Cart;
                   $data->product_id = $request->product;
                   $data->no_of_items = $request->items;
                   $data->price      = $request->items * $product->sale_price;
                   if($data->save())
                   {
                    $data = [
                        'response' => 1,
                        'message'  => 'Product Added to cart',
                        'class'    => 'alert alert-success',
                    ];
                    return response()->json($data);

                   }
                }
           }
           

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
        $products = Cart::with('products')->get();
        return view('pages.Cart.product-list', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }

    public function clear_cart()
    {
        $clear_cart = Cart::truncate();
        if($clear_cart)
        {
            $data = [
                'response' => 1
            ];
            return response()->json($data);
        }
    }
}
